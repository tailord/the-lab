//
//.:..:..:..:..:..:..:..:.
// Tailor'd Design
// jesse@tailord.design
//.:..:..:..:..:..:..:..:.
//

import React, { useState, useEffect, useContext } from 'react';
import GlobalState from '../Contexts/GlobalState';
// import $ from 'jquery';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMedal } from '@fortawesome/free-solid-svg-icons';

import warzoneData from '../Connectors/rapidAPIWarzone';
// import jsonbin from '../Connectors/jsonbin';
import firebase from '../Connectors/firebase';
import combineStats from '../Functions/combineStats';

/* 
- on load set a stall count, then fetch stats (rate limiting) (@todo might be able to ignore this if choosing to go local right off the bat?)
- the fetch will check on 'last fetched date' and if its > 15min then go directly to rapidAPI for new stats, if its < 15min, then fetch locally (Firebase), then clear fake/loading data
- if fetching from rapidAPI, then update local API
- once data is fetched, then group and combine stats for display
*/

const loadingData = {
	kills: 0,
	objectiveTeamWiped: 0,
	objectiveLastStandKill: 0,
	wallBangs: 0,
	avgLifeTime: 0,
	objectivePlunderCashBloodMoney: 0,
	score: 0,
	headshots: 0,
	assists: 0,
	killsPerGame: 0,
	scorePerMinute: 0,
	distanceTraveled: 0,
	deaths: 0,
	objectiveMunitionsBoxTeammateUsed: 0,
	objectiveBrDownEnemyCircle2: 0,
	kdRatio: 0,
	objectiveBrDownEnemyCircle1: 0,
	objectiveBrMissionPickupTablet: 0,
	objectiveReviver: 0,
	objectiveBrKioskBuy: 0,
	gulagDeaths: 0,
	timePlayed: 0,
	headshotPercentage: 0,
	executions: 0,
	matchesPlayed: 0,
	gulagKills: 0,
	nearmisses: 0,
	objectiveBrCacheOpen: 0,
	damageDone: 0,
	damageTaken: 0,
	downs: 0,
};
let t = new Date();
let cache_time = 30;

const StatCard = ({
	warrior,
	info,
	type,
	stall,
	maxPlayers,
	types,
	evaluateStats,
	forceFetch,
}) => {
	// const [global, setGlobal] = useContext(GlobalState);
	const [global] = useContext(GlobalState);
	const [loading, setLoading] = useState('is-loading');
	// const [seeAllTxt, setSeeAllTxt] = useState('See All...');
	const [stallCount, setStallCount] = useState();
	const [stats, setStats] = useState(loadingData);
	const [lastDate, setLastDate] = useState(global['lastDate']);

	useEffect(() => {
		setStallCount(stall * 2000);
	}, []);

	useEffect(() => {
		if (stallCount !== undefined) {
			console.info('stallcount found!');
			if (lastDate > cache_time) {
				setTimeout(fetchStats, stallCount);
			} else {
				fetchStats();
			}
		} else {
			console.info(stallCount, 'why no stallcount ' + warrior + '..' + stall);
		}
	}, [stallCount]);

	useEffect(() => {
		if (forceFetch) {
			setLoading('is-loading');
			setTimeout(fetchStats, stallCount);
		}
	}, [forceFetch]);

	useEffect(() => {
		evaluateStats(stats, warrior);
	}, [stats]);

	const fetchStats = () => {
		// rapidAPIWarzone.getData(this.props.warrior,this.props.info['hash'],'weekly-stats',this.props.info['platform']).then((response) =>  {
		if (lastDate > cache_time || forceFetch) {
			console.info(t - lastDate, 'fetching from rapidAPI...');
			warzoneData
				.getData('weekly-stats', info['url'], 'weekly-stats')
				.then((response) => {
					// rapidAPIWarzone.getData('Scruffy Lefty','','weekly-stats','xbl').then((response) => {
					setStats(combineStats(response.data, types));
					// setStats(response.data);
					postStats(response.data);
					setLoading('');
					document
						.getElementById('msg--last-fetched')
						.classList.add('fetching');
					console.info('fetched from rapidAPI!');
				});
		} else {
			console.info('fetching locally...');
			setStats(combineStats(global[warrior], types));
			// setStats(global[warrior]);
			// document.getElementById('msg--last-fetched').classList.add('locally');
			setLoading('');
			console.info('fetched locally!');
			// const {data, status} = await jsonbin.fetch();
			// if (status === 200) {
			// 	setStats(data['record'][warrior]);
			// 	setLoading('');
			// 	console.info('fetched locally!');
			// }
		}
	};

	const postStats = async (stats) => {
		// fetch all data to overwrite appropriately.. @todo this should be done once, not on each player..
		global['timestamp'] = t.getTime();
		global[warrior] = stats;
		// await jsonbin.update(global);
		await firebase.update(global);
	};

	// const showAll = () => {
	// 	$('#show-all-stats').toggle();
	// 	if(seeAllTxt === 'See All...'){
	// 		setSeeAllTxt('Hide');
	// 	} else {
	// 		setSeeAllTxt('See All...');
	// 	}
	// }

	const comma = (n) => {
		return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
	};

	// if (stats) {
	// 	stat_group = combineStats();
	// }

	return (
		<div className="card--stats" data-warrior={warrior}>
			<h2>
				{warrior} <span className={loading}></span>
			</h2>
			<h3 data-stat="kdRatio">
				K/D: <span className={loading}>{stats['kdRatio'].toFixed(2)}</span>
				<FontAwesomeIcon icon={faMedal} style={{ display: 'none' }} />
			</h3>
			<h3 data-stat="kills">
				Kills: <span className={loading}>{stats['kills']}</span>
				<FontAwesomeIcon icon={faMedal} style={{ display: 'none' }} />
			</h3>
			<h3 data-stat="downs">
				Downs: <span className={loading}>{stats['downs']}</span>
				<FontAwesomeIcon icon={faMedal} style={{ display: 'none' }} />
			</h3>
			<h3 data-stat="deaths">
				Deaths: <span className={loading}>{stats['deaths']}</span>
				<FontAwesomeIcon icon={faMedal} style={{ display: 'none' }} />
			</h3>
			<h3 data-stat="assists">
				Assists: <span className={loading}>{stats['assists']}</span>
				<FontAwesomeIcon icon={faMedal} style={{ display: 'none' }} />
			</h3>
			<h3 data-stat="objectiveReviver">
				Revives: <span className={loading}>{stats['objectiveReviver']}</span>
				<FontAwesomeIcon icon={faMedal} style={{ display: 'none' }} />
			</h3>
			<h3 data-stat="objectiveBrMissionPickupTablet">
				Contracts Started:{' '}
				<span className={loading}>
					{stats['objectiveBrMissionPickupTablet']}
				</span>
				<FontAwesomeIcon icon={faMedal} style={{ display: 'none' }} />
			</h3>
			<h3 data-stat="score">
				Score: <span className={loading}>{comma(stats['score'])}</span>
				<FontAwesomeIcon icon={faMedal} style={{ display: 'none' }} />
			</h3>
			<h3 data-stat="killsPerGame">
				Kills per Game:{' '}
				<span className={loading}>{stats['killsPerGame'].toFixed(2)}</span>
				<FontAwesomeIcon icon={faMedal} style={{ display: 'none' }} />
			</h3>
			<h3 data-stat="gulagKills">
				Gulag Wins: <span className={loading}>{stats['gulagKills']}</span>
				<FontAwesomeIcon icon={faMedal} style={{ display: 'none' }} />
			</h3>
			<h3 data-stat="headshots">
				Headshots: <span className={loading}>{stats['headshots']}</span>
				<FontAwesomeIcon icon={faMedal} style={{ display: 'none' }} />
			</h3>
			<h3 data-stat="headshotPercentage">
				Headshot %:{' '}
				<span className={loading}>
					{stats['headshotPercentage'].toFixed(2)}
				</span>
				<FontAwesomeIcon icon={faMedal} style={{ display: 'none' }} />
			</h3>
			<h3 data-stat="damageDone">
				Damage: <span className={loading}>{comma(stats['damageDone'])}</span>
				<FontAwesomeIcon icon={faMedal} style={{ display: 'none' }} />
			</h3>
			<h3 data-stat="damageTaken">
				Damage Taken:{' '}
				<span className={loading}>{comma(stats['damageTaken'])}</span>
				<FontAwesomeIcon icon={faMedal} style={{ display: 'none' }} />
			</h3>
			<h3 data-stat="matchesPlayed">
				Matches: <span className={loading}>{stats['matchesPlayed']}</span>
				<FontAwesomeIcon icon={faMedal} style={{ display: 'none' }} />
			</h3>
			<h3 data-stat="avgLifeTime">
				Avg Life:{' '}
				<span className={loading}>{stats['avgLifeTime'].toFixed(2)}</span>
				<FontAwesomeIcon icon={faMedal} style={{ display: 'none' }} />
			</h3>
			{/*<button id="show-all-stats-btn" onClick={>{seeAllTxt}</button>*/}
			<div id="show-all-stats" style={{ display: 'none' }}>
				{Object.keys(stats).map((val, i) => (
					<div key={i}>
						<strong>{val.replace(/([a-z])([A-Z])/g, `$1 $2`)}:</strong>
						<span>{stats[val].toFixed(2)}</span>
					</div>
				))}
			</div>
		</div>
	);
};

export default StatCard;
