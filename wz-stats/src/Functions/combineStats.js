//
//.:..:..:..:..:..:..:..:.
// Tailor'd Design
// jesse@tailord.design
//.:..:..:..:..:..:..:..:.
//

// stats for averages need to be after the stats needed for the average
// const br_modes = ['br_brsolo','br_brduos','br_brtrios','br_brquads', 'br_rebirth_resurgence_mini', 'br_rebirth_rbrthquad', 'br_dbd_dbd'];
// const br_modes = [
// 	'br_vg_royale_trios',
// 	'br_rebirth_cal_res_royale',
// 	'br_rebirth_vg_res_44',
// 	'br_rebirth_rbrthtrios',
// ];
const br_modes = ['br_all'];
// const br_modes = ['br_rebirth_rbrthquad'];

const combineStats = (statdata, stat_types) => {
	let combined = {};
	let statParent = statdata['wz']['mode'];
	// set everything to 0, for math!
	for (var d = 0; d < stat_types.length; d++) {
		combined[stat_types[d]] = 0;
	}
	for (var i = 0; i < br_modes.length; i++) {
		let mode = br_modes[i];
		if (statParent[mode]) {
			for (var j = 0; j < stat_types.length; j++) {
				let statName = stat_types[j];
				let cval = combined[statName];
				let nval = 0;
				switch (statName) {
					// case 'kdRatio':
					// 	nval = combined['kills'] / combined['deaths'];
					// 	break;
					// case 'killsPerGame':
					// 	nval = combined['kills'] / combined['matchesPlayed'];
					// 	break;
					// case 'headshotPercentage':
					// 	nval = combined['headshots'] / combined['kills'];
					// 	break;
					// case 'downs':
					// 	let circleDowns = 0;
					// 	for (var c = 1; c < 8; c++) {
					// 		let circle =
					// 			statParent[mode]['properties'][
					// 				'objectiveBrDownEnemyCircle' + c
					// 			];
					// 		if (circle) {
					// 			circleDowns += circle;
					// 		}
					// 	}
					// 	nval = cval + circleDowns;
					// 	break;
					// case 'avgLifeTime':
					// 	nval = combined['avgLifeTime'] / combined['matchesPlayed'];
					// 	break;
					default:
						if (!statParent[mode]['properties'][statName]) {
							break;
						} else {
							nval = cval + statParent[mode]['properties'][statName];
						}
				}

				combined[statName] = nval;
			}
		}
	}
	return combined;
};

export default combineStats;
