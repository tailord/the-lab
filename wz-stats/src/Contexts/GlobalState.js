//
//.:..:..:..:..:..:..:..:.
// Tailor'd Design
// jesse@tailord.design
//.:..:..:..:..:..:..:..:.
//


import React from 'react';

const GlobalState = React.createContext([{}, () => {}]);

export default GlobalState;