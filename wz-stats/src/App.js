//
//.:..:..:..:..:..:..:..:.
// Tailor'd Design
// jesse@tailord.design
//.:..:..:..:..:..:..:..:.
//

import React, { useState, useEffect } from 'react';
import GlobalState from './Contexts/GlobalState';
import $ from 'jquery';

import './styles/_styles.scss';
import StatCard from './Components/statCard';
// import jsonbin from './Connectors/jsonbin';
import firebase from './Connectors/firebase';

const version = '1.1.5';
const note = 'All modes!';

const players = {
	crowexx: {
		hash: '1210',
		platform: 'battle',
		url: '/crowexx%25231210/battle',
	},
	'Scruffy Lefty': {
		hash: '',
		platform: 'xbl',
		url: '/Scruffy%20Lefty/xbl',
	},
	Milkman: {
		hash: '',
		platform: 'psn',
		url: '/robots19/psn',
	},
	AlbertPooHoles: {
		hash: '',
		platform: 'xbl',
		url: '/KillABraY3776/xbl',
	},
	SlayJ: {
		hash: '',
		platform: 'xbl',
		url: '/Gretekins/xbl',
	},
};

const playerListCount = Object.keys(players).length;
const monthNames = [
	'Jan',
	'Feb',
	'Mar',
	'Apr',
	'May',
	'Jun',
	'Jul',
	'Aug',
	'Sept',
	'Oct',
	'Nov',
	'Dec',
];
const stat_types = [
	'kills',
	'deaths',
	'assists',
	'score',
	'gulagKills',
	'headshots',
	'damageDone',
	'damageTaken',
	'matchesPlayed',
	'kdRatio',
	'killsPerGame',
	'headshotPercentage',
	// 'objectiveBrMissionPickupTablet',
	// 'objectiveReviver',
	'downs',
	'avgLifeTime',
];

let topScores = {};

const App = () => {
	const [global, setGlobal] = useState({});
	const [timeAgo, setTimeAgo] = useState();
	const [week, setWeek] = useState();
	const [isFetched, setIsFetched] = useState(false);
	const [forceFetch, setForceFetch] = useState(false);

	const getWeek = () => {
		const d = new Date();
		const back = d.getDate() - 6;
		const today = d.getDate();
		const month = d.getMonth();
		let week = monthNames[month] + ' ' + back + ' - ' + today;
		if (today < 7) {
			// find previous month's day via UTC milliseconds
			const backback = new Date(d - 6 * 1000 * 60 * 60 * 24);
			week =
				monthNames[month - 1] +
				' ' +
				backback.getDate() +
				' - ' +
				monthNames[month] +
				' ' +
				today;
		}
		setWeek(week);
	};

	const evaluateStats = (stats, warrior) => {
		// this runs after the 1st user
		const lowerStats = ['damageTaken', 'deaths'];
		if (Object.keys(topScores).length > 0 && Object.keys(players).length > 1) {
			// if
			for (var i = 0; i < stat_types.length; i++) {
				let estat = stat_types[i];
				if (stats[estat] === 0) {
					continue;
				}
				if (stats[estat] > topScores[estat] && !lowerStats.includes(estat)) {
					topScores[estat] = stats[estat];
					highlightPoints(warrior, estat);
				} else if (lowerStats.includes(estat)) {
					if (stats[estat] < topScores[estat]) {
						topScores[estat] = stats[estat];
						highlightPoints(warrior, estat);
					}
				}
			}
		} else {
			topScores = stats;
			for (var j = 0; j < stat_types.length; j++) {
				highlightPoints(warrior, stat_types[j]);
			}
		}
	};

	const highlightPoints = (warrior, stat) => {
		$('[data-stat="' + stat + '"]').removeClass('top-score');
		$('[data-warrior="' + warrior + '"]')
			.find('[data-stat="' + stat + '"]')
			.addClass('top-score');
	};

	const playerCards = () => {
		if (timeAgo) {
			return Object.keys(players).map((player, i) => (
				<StatCard
					warrior={player}
					info={players[player]}
					type="wz"
					key={i}
					stall={i}
					maxPlayers={playerListCount}
					types={stat_types}
					evaluateStats={evaluateStats}
					forceFetch={forceFetch}
				/>
			));
		} else {
			return '...';
		}
	};

	const fetchOgStats = async () => {
		// const {data, status} = await jsonbin.fetch();
		const fdata = await firebase.fetch();
		setGlobal(fdata.docs[0].data());
		setIsFetched(true);
		// if (status === 200) {
		// setGlobal(data['record']);
		// }
	};

	const findTimeAgo = () => {
		let somdate = global['timestamp'];
		let time = new Date();
		time = time.getTime();
		let timediff = time - somdate;
		timediff = (timediff / 1000 / 60).toFixed(2);
		setGlobal((global) => ({ ...global, lastDate: Number(timediff) }));
		setGlobal((global) => ({ ...global, timeMsg: timediff + ' min ago.' }));
		// global['lastDate'] = Number(timediff);
		setTimeAgo(timediff);
	};

	const forceFetchHandle = (e) => {
		if (e.target.disabled === false) {
			e.target.disabled = true;
			$('[data-stat]').removeClass('top-score');
			setForceFetch(true);
		}
	};

	useEffect(() => {
		fetchOgStats();
		getWeek();
	}, []);

	useEffect(() => {
		// use timestamp since this will only trigger once
		if (global.timestamp) {
			findTimeAgo();
		}
	}, [isFetched]);

	return (
		<GlobalState.Provider value={[global, setGlobal]}>
			<div className="App">
				<header>
					<div className="title-bar">
						<h1>WAR BOIS</h1>
						<button className="sm" onClick={(e) => forceFetchHandle(e)}>
							Fetch New
						</button>
					</div>
					<span className="green">{week}</span>
					<br />
					<pre style={{ fontSize: '12px' }}>
						<em id="msg--last-fetched">
							Last Fetched: {global['lastDate'] ? global['timeMsg'] : '...'}
						</em>
						<br />
						<em>{note}</em>
					</pre>
				</header>
				<div className="list--cards">{playerCards()}</div>
				{/*<div className="list--cards">{Object.keys(staticStats).map((stat,i) => <div key={i}>{stat}</div>)}</div>*/}
			</div>
			<footer>
				crowexxgaming@gmail.com
				<br />v{version}
			</footer>
		</GlobalState.Provider>
	);
};

export default App;
