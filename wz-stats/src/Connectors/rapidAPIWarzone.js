//
//.:..:..:..:..:..:..:..:.
// Tailor'd Design
// jesse@tailord.design
//.:..:..:..:..:..:..:..:.
//


import axios from 'axios';

// Create instance called instance
const instance = axios.create({
    baseURL: 'https://call-of-duty-modern-warfare.p.rapidapi.com',
    headers: {
		'x-rapidapi-key': process.env.REACT_APP_RAPIDAPI_KEY,
		'x-rapidapi-host': 'call-of-duty-modern-warfare.p.rapidapi.com'
	},
});

// gamertag = name#number (use %2523 for #)
// platform = psn, steam, xbl, battle, uno (Activision ID), acti (Activision Tag)
// /warzone/
// /warzone-matches/

const warzoneData = {
    getData: (type, url, tag, hash, platform) =>
    instance({
        'method':'GET',
        // 'url':'/'+type+'/'+tag+'%2523'+hash+'/'+platform,
        // 'url':'/'+type+'/'+tag+'/'+platform,
        'url':'/'+type+url,
        transformResponse: [function (data) {
            // Do whatever you want to transform the data
            // console.log('Transforming data...Loading...'+ tag + type);
            // console.log(data, typeof data); //object {br:{},br_all:{},...}
            let json = JSON.parse(data);
            //// list of nested object keys
            //// let stats = Object.keys(json);
            //let stats = data;
            // console.log(stats);
            //// data = {
            ////     stats
            //// }
            return json;
        }],
    })
}

export default warzoneData;