//
//.:..:..:..:..:..:..:..:.
// Tailor'd Design
// jesse@tailord.design
//.:..:..:..:..:..:..:..:.
//


import axios from 'axios';

const jbinURL = 'https://api.jsonbin.io/v3/b/603fba3d81087a6a8b958f97';
const sk = process.env.REACT_APP_JSONBIN_KEY;

// Create instance called instance
const instance = axios.create({
    baseURL: jbinURL,
    headers: {
		'Content-Type':'application/json',
        'X-Master-Key': sk
	}
});

// const jsonbin = {
//     putData:(stats) =>
//     instance({
//         'method':'PUT',
//         // 'url':'/b/603fba3d81087a6a8b958f97',
//         'data':stats
//     })
// }

const update = stats => {
    return instance({
        'method':'PUT',
        'data':stats
    })
}

const fetch = () => {
    return instance({
        'method':'GET'
    })
}

const exportobjects = {
    update,
    fetch
}

export default exportobjects;