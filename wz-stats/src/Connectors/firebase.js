//
//.:..:..:..:..:..:..:..:.
// Tailor'd Design
// jesse@tailord.design
//.:..:..:..:..:..:..:..:.
//


import firebase from 'firebase/app';
import 'firebase/firestore';

const config = {
    apiKey: process.env.REACT_APP_FIREBASE_KEY,
    authDomain: process.env.REACT_APP_FIREBASE_DOMAIN,
    databaseURL: "https://warzone-stats-e65c7-default-rtdb.firebaseio.com",
    projectId: "warzone-stats-e65c7",
    storageBucket: "warzone-stats-e65c7.appspot.com",
    messagingSenderId: process.env.REACT_APP_FIREBASE_MSGID,
    appId: process.env.REACT_APP_FIREBASE_APPID
};
// Initialize Firebase
firebase.initializeApp(config);
const db = firebase.firestore();
const collection = 'all-wz-stats';

const fetch = () => {
    return db.collection(collection).get();
}

const update = stats => {
    db.collection(collection).doc('0').set(stats);
    // or .set({...stats, <warrior>:<data>}) for just updating that warrior
}

const exportobjects = {
    fetch,
    update
}

export default exportobjects;