//
//.:..:..:..:..:..:..:..:.
// Tailor'd Design
// jesse@tailord.design
//.:..:..:..:..:..:..:..:.
//
// @todo
//

<div id="block-formblock-contact-site" class="block block-formblock first odd">
					<h2 class="block__title block-title">Get In Touch</h2>

					<form class="user-info-from-cookie contact-form" action="/" method="post" id="contact-site-form" accept-charset="UTF-8">
						<div>
							<div class="form-item form-type-textfield form-item-name">
								<label for="edit-name">Your name <span class="form-required" title="This field is required.">*</span></label>
								<input type="text" id="edit-name" name="name" value="" size="60" maxlength="255" class="form-text required">
							</div>
							<div class="form-item form-type-textfield form-item-mail">
								<label for="edit-mail">Your email <span class="form-required" title="This field is required.">*</span></label>
								<input type="text" id="edit-mail" name="mail" value="" size="60" maxlength="255" class="form-text required">
							</div>
							<div class="form-item form-type-textfield form-item-subject">
								<label for="edit-subject">Subject <span class="form-required" title="This field is required.">*</span></label>
								<input type="text" id="edit-subject" name="subject" value="" size="60" maxlength="255" class="form-text required">
							</div>
							<div class="form-item form-type-textarea form-item-message">
								<label for="edit-message">Message <span class="form-required" title="This field is required.">*</span></label>
								<div class="form-textarea-wrapper resizable textarea-processed resizable-textarea"><textarea id="edit-message" name="message" cols="60" rows="5" class="form-textarea required"></textarea>
									<div class="grippie"></div>
								</div>
							</div>
							<input type="hidden" name="form_build_id" value="form-zYufn_PTjFo1A020NSMDN38Uizfi6yS0kn1VILjwxdI">
							<input type="hidden" name="form_id" value="contact_site_form">
							<div class="form-actions form-wrapper" id="edit-actions">
								<input type="submit" id="edit-submit" name="op" value="Send message" class="form-submit">
							</div>
						</div>
					</form>
					</div>