// Drupal dump


export const DrupalContent =
{
  "nodes" : [
    {
      "node" : {
        "nid" : "1",
        "type" : "Project",
        "title" : "Fabolous Week",
        "field_hero" : {
          "src" : "http://tailord.design/sites/default/files/styles/large/public/page_hero/new-balance-2.jpg?itok=6tkUngm6",
          "alt" : ""
        },
        "field_hero_caption" : "Join the Movement",
        "field_overlay_rgb" : "128, 161, 180",
        "field_client" : "VILLA",
        "body" : "These were unique pages and mini-sites built to compliment marketings campaign endeavors. In the rapid retail environment VILLA needed to keep the website fresh with up-to-date content and special buying experiences online.\n",
        "field_hand_offs" : "Ecommerce, Magento, Web Development, Web Design, UI, UX",
        "field_project_image" : [
          {
            "src" : "http://tailord.design/sites/default/files/project_portfolios/fab-hero-1200.jpg",
            "alt" : ""
          },
          {
            "src" : "http://tailord.design/sites/default/files/project_portfolios/main-new-balance-800.jpg",
            "alt" : ""
          },
          {
            "src" : "http://tailord.design/sites/default/files/project_portfolios/timberland-2-800.jpg",
            "alt" : ""
          },
          {
            "src" : "http://tailord.design/sites/default/files/project_portfolios/kendrick-1200.jpg",
            "alt" : ""
          },
          {
            "src" : "http://tailord.design/sites/default/files/project_portfolios/fab-starter-1200.jpg",
            "alt" : ""
          },
          {
            "src" : "http://tailord.design/sites/default/files/project_portfolios/fab-starter-jackets-1200.jpg",
            "alt" : ""
          },
          {
            "src" : "http://tailord.design/sites/default/files/project_portfolios/timberland-5-800.jpg",
            "alt" : ""
          },
          {
            "src" : "http://tailord.design/sites/default/files/project_portfolios/fab-grid-800.jpg",
            "alt" : ""
          }
        ],
        "promote" : "Yes"
      }
    },
    {
      "node" : {
        "nid" : "2",
        "type" : "Basic page",
        "title" : "Home",
        "field_hero" : {
          "src" : "http://tailord.design/sites/default/files/styles/large/public/page_hero/flowervintage.jpg?itok=hYJiy7t0",
          "alt" : ""
        },
        "field_hero_caption" : "",
        "field_overlay_rgb" : "",
        "field_client" : "",
        "body" : "\n\nSolutions that fit.™Tailor'd Design is a creative agency.We develop and build brand strategies that elevate your story and make you look awesome.\n\n",
        "field_hand_offs" : "",
        "field_project_image" : "",
        "promote" : "No"
      }
    },
    {
      "node" : {
        "nid" : "3",
        "type" : "Project",
        "title" : "Ecommerce Overhaul",
        "field_hero" : {
          "src" : "http://tailord.design/sites/default/files/styles/large/public/page_hero/hero-villa2.jpg?itok=NBP0a4cQ",
          "alt" : ""
        },
        "field_hero_caption" : "VILLA 2.0",
        "field_overlay_rgb" : "34,34,34",
        "field_client" : "VILLA",
        "body" : "A complete overhaul from their previous Ecommerce site brought a brand new, fresh experience for their shoppers. This clean, concise design presents users with direct content and streamlined checkout.\n",
        "field_hand_offs" : "Ecommerce, Magento, Web Development, Web Design, UX",
        "field_project_image" : [
          {
            "src" : "http://tailord.design/sites/default/files/project_portfolios/project-villa2-hp-desktop-1200_1.jpg",
            "alt" : ""
          },
          {
            "src" : "http://tailord.design/sites/default/files/project_portfolios/project-villa2-mobile-1200.jpg",
            "alt" : ""
          },
          {
            "src" : "http://tailord.design/sites/default/files/project_portfolios/jordan-800.jpg",
            "alt" : ""
          },
          {
            "src" : "http://tailord.design/sites/default/files/project_portfolios/purple-boot-800.jpg",
            "alt" : ""
          },
          {
            "src" : "http://tailord.design/sites/default/files/project_portfolios/921948-600-2-800.jpg",
            "alt" : ""
          },
          {
            "src" : "http://tailord.design/sites/default/files/project_portfolios/310810-100-800.jpg",
            "alt" : ""
          },
          {
            "src" : "http://tailord.design/sites/default/files/project_portfolios/releases-page-1200.jpg",
            "alt" : ""
          }
        ],
        "promote" : "Yes"
      }
    },
    {
      "node" : {
        "nid" : "4",
        "type" : "Project",
        "title" : "Graphic Work",
        "field_hero" : {
          "src" : "http://tailord.design/sites/default/files/styles/large/public/page_hero/vx-cloud-tunnel-1200.jpg?itok=4KWPJypr",
          "alt" : ""
        },
        "field_hero_caption" : "VISUAL XFER",
        "field_overlay_rgb" : "48,72,84",
        "field_client" : "V/X",
        "body" : "Trying to push the bounds of creativity in my mind I started an Instagram account where I'd post different ideas somewhat frequently. I called it 'Visual Xfer' because it was moving those images in my head into the head of another viewer. Most of these were done from different iPhone apps, and some of the photos were my own.\nᐧᐃᐧ\n",
        "field_hand_offs" : "Graphic Design, Photography",
        "field_project_image" : [
          {
            "src" : "http://tailord.design/sites/default/files/project_portfolios/vx-skyd-1200.jpg",
            "alt" : ""
          },
          {
            "src" : "http://tailord.design/sites/default/files/project_portfolios/vx-balloons-800.jpg",
            "alt" : ""
          },
          {
            "src" : "http://tailord.design/sites/default/files/project_portfolios/vx-bwtree-800.jpg",
            "alt" : ""
          },
          {
            "src" : "http://tailord.design/sites/default/files/project_portfolios/vx-skyforce-800.jpg",
            "alt" : ""
          },
          {
            "src" : "http://tailord.design/sites/default/files/project_portfolios/vx-mtntop-800.jpg",
            "alt" : ""
          },
          {
            "src" : "http://tailord.design/sites/default/files/project_portfolios/vx-celeste-800.jpg",
            "alt" : ""
          },
          {
            "src" : "http://tailord.design/sites/default/files/project_portfolios/vx-pusher-800.jpg",
            "alt" : ""
          },
          {
            "src" : "http://tailord.design/sites/default/files/project_portfolios/vx-chameleon-800.jpg",
            "alt" : ""
          },
          {
            "src" : "http://tailord.design/sites/default/files/project_portfolios/vx-astrobrige-800.jpg",
            "alt" : ""
          }
        ],
        "promote" : "No"
      }
    },
    {
      "node" : {
        "nid" : "5",
        "type" : "Project",
        "title" : "Custom UX",
        "field_hero" : {
          "src" : "http://tailord.design/sites/default/files/styles/large/public/page_hero/project-tara-mobile-1200.jpg?itok=OdLjCmif",
          "alt" : ""
        },
        "field_hero_caption" : "Fearless Imagination",
        "field_overlay_rgb" : "56, 14, 132",
        "field_client" : "Tara Wilson Agency",
        "body" : "Tara Wilson Agency brings the POW to their clients events, but their previous website was not representing themselves at that same level of professionalism. We worked with TWA to build them a custom web experience from their design direction. A great example of collaboration and mutual ambition to make things awesome.\nBuilt with Drupal, TWA has custom fields to manage their extensive work portfolio, team member roster, job listings and other cms functions.\n",
        "field_hand_offs" : "Drupal, Web Development, UX",
        "field_project_image" : [
          {
            "src" : "http://tailord.design/sites/default/files/project_portfolios/project-tara-desktop2-1200.jpg",
            "alt" : ""
          },
          {
            "src" : "http://tailord.design/sites/default/files/project_portfolios/project-tara-desktop3-800.jpg",
            "alt" : ""
          },
          {
            "src" : "http://tailord.design/sites/default/files/project_portfolios/project-tara-desktop4-800.jpg",
            "alt" : ""
          }
        ],
        "promote" : "No"
      }
    },
    {
      "node" : {
        "nid" : "6",
        "type" : "Project",
        "title" : "Shopify in a Snap",
        "field_hero" : {
          "src" : "http://tailord.design/sites/default/files/styles/large/public/page_hero/about-us.jpg?itok=v7ggUl-g",
          "alt" : ""
        },
        "field_hero_caption" : "A Voice in the Streets",
        "field_overlay_rgb" : "136, 248, 255",
        "field_client" : "Identity Politics",
        "body" : "Identity Politics had a vision and needed a platform in a hurry to get their message out. We had them up and running and selling their apparel in less than week and sales started flowing in.\n",
        "field_hand_offs" : "Ecommerce, Shopify",
        "field_project_image" : {
          "src" : "http://tailord.design/sites/default/files/project_portfolios/project-identity-politics-desktop-1200.jpg",
          "alt" : ""
        },
        "promote" : "No"
      }
    },
    {
      "node" : {
        "nid" : "7",
        "type" : "Project",
        "title" : "Wildwood 2018",
        "field_hero" : {
          "src" : "http://tailord.design/sites/default/files/styles/large/public/page_hero/group-1200.jpg?itok=hWQ9NVcm",
          "alt" : ""
        },
        "field_hero_caption" : "Get Kraken",
        "field_overlay_rgb" : "206,57,99",
        "field_client" : "Red Hot Tacos",
        "body" : "For our annual Wildwood Ultimate tournament we designed new team jerseys for the Red Hot Tacos. Fully sublimated, full of color. We had a local Philly company print the jerseys for us (as well as tees and long sleeve jerseys) over at Pink's Inks.\n",
        "field_hand_offs" : "Graphic Design",
        "field_project_image" : [
          {
            "src" : "http://tailord.design/sites/default/files/project_portfolios/justin-sky-1200.jpg",
            "alt" : ""
          },
          {
            "src" : "http://tailord.design/sites/default/files/project_portfolios/eric-800.jpg",
            "alt" : ""
          },
          {
            "src" : "http://tailord.design/sites/default/files/project_portfolios/jesse-800.jpg",
            "alt" : ""
          },
          {
            "src" : "http://tailord.design/sites/default/files/project_portfolios/tyler-stretch-1200.jpg",
            "alt" : ""
          },
          {
            "src" : "http://tailord.design/sites/default/files/project_portfolios/will-800.jpg",
            "alt" : ""
          },
          {
            "src" : "http://tailord.design/sites/default/files/project_portfolios/group-800.jpg",
            "alt" : ""
          }
        ],
        "promote" : "Yes"
      }
    },
    {
      "node" : {
        "nid" : "8",
        "type" : "Webform",
        "title" : "Tax Request Test",
        "field_hero" : "",
        "field_hero_caption" : "",
        "field_overlay_rgb" : "",
        "field_client" : "",
        "body" : "",
        "field_hand_offs" : "",
        "field_project_image" : "",
        "promote" : "No"
      }
    }
  ]
}