//
//.:..:..:..:..:..:..:..:.
// Tailor'd Design
// jesse@tailord.design
//.:..:..:..:..:..:..:..:.
//

import React, {Component} from 'react';

class BlockServices extends Component {
	
	render() {
		return (
			<div className="services logo-list" data-fade-this>
				<div className="service">
					<span className="icon icon--circle icon--photo"></span>
				</div>
				<div className="service">
					<span className="icon icon--circle icon--brush"></span>
				</div>
				<div className="service">
					<span className="icon icon--circle icon--code"></span>
				</div>
			</div>
		);
	}
}


export default BlockServices;