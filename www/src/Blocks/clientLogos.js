//
//.:..:..:..:..:..:..:..:.
// Tailor'd Design
// jesse@tailord.design
//.:..:..:..:..:..:..:..:.
//
// @todo
//

import React, {Component} from 'react';

class BlockClientLogos extends Component {
	
	render() {
		return (
			<div className="services logo-list">
				<div className="service">
					<span className="icon icon--circle icon--photo"></span>
				</div>
				<div className="service">
					<span className="icon icon--circle icon--brush"></span>
				</div>
				<div className="service">
					<span className="icon icon--circle icon--code"></span>
				</div>
			</div>
		);
	}
}


export default BlockClientLogos;