## Tailor'd React site

This is currently a WIP. Content is fed from Contentful.

@todo

- Contact form


## Deploying updates

Use `s3cmd` [usage](https://s3tools.org/usage) / [how-to](https://s3tools.org/s3cmd-howto)

- Compile and build code with `npm run build`
- `cd` to `/build` directory
- Run `s3cmd put * --recursive s3://tailord.design` to upload all of the compiled new build. Alternatively, single out a file or directory instead of `*`.