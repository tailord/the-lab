/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {


// To understand behaviors, see https://drupal.org/node/756722#behaviors
Drupal.behaviors.my_custom_behavior = {
  attach: function(context, settings) {

    // Place your code here.

	var isMobile = (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent));

    function getAccentColor(image, callback) {
		image.onload = function(){
		    var colorThief = new ColorThief();
		    var color = colorThief.getColor(image);
		    var palette = colorThief.getPalette(image);
		    console.log(color);
		    // $('.opening .bg-color').css('background-color','rgba('+palette[8]+', 0.2)');

		    callback(palette);
		};	
	}

	// function fixedArticle() {
	// 	var $article = $('.project-view > article');
	// 		// $articleFades = $article.find('p, button').addClass('data-fading-block');
	// 	// setTimeout(function() {
	// 	// 	fader($articleFades, 0.75);
	// 	// }, 1200);
	// 	if (!isMobile) {
	// 		$(window).scroll(function(){
	// 			var wH = $(this).height();
	// 			h = (wH * 0.1) + ($('.hero.opening').height());
	// 			a = (window.pageYOffset > h);
	// 			$article.toggleClass('fixed', a);
	// 			// if (a) {$article.css({top:window.pageYOffset-h});}
	// 			// else {$article.css({top:0});}
	// 		});
	// 	}
	// }

	function fader(el, buf) {
		el.each(function() {
			el.addClass('data-fading-block');
			var t = $(this).offset().top;
			var _lock = false;
			var $e = $(this);
			$(window).scroll(function(){
				var wH = $(this).height();
				var	trigger = t - (wH * buf);
					s = window.pageYOffset;
				// console.log(s+','+t+','+trigger+','+_lock);
				if ((s > trigger) && !_lock) {
					$e.addClass('fade-in');
					_lock = true;
					// console.log(_lock+'2');
				} else if ((s < trigger) && _lock) {
					$e.removeClass('fade-in');
					_lock = false;
					// console.log(_lock+'3');
				}
			});
		});
	}

	function initProjectView() {
		$('.project').on('click', function() {
			var nid = $(this).find('[data-load-nid]').attr('data-load-nid');
			$('html, body').animate({scrollTop:0}, 500, function() {
				$('#site-main').addClass('fade');
			});
			$('.project-card').load('/node/'+nid+' #node-'+nid, function() {
				$('.project-card').addClass('show');
				//allows card to be clicked and not close
				$(this).find('.node-project').on('click', function(e) {
					e.stopPropagation();
				});
			});
			$('#site-main').on('click', function() {
				$(this).removeClass('fade').find('.project-card').removeClass('show').html('');
			});
		});
	}

	$(document).ready(function() {
		fader($('.services'), 0.68);
		$('.icon--nav-anim').on('click', function() {
			$(this).toggleClass('open');
			$('.menu-block-1').toggleClass('open').promise().done(function(){
				setTimeout(function() {
					$('.menu-block-1').toggleClass('animate');
				}, 200);
			});
		});
		$('#block-menu-block-1 .menu__item a').on('click', function() {
			$(this).parents('.menu-block-1').removeClass('open animate');
			$('.icon--nav-anim').removeClass('open');
		});


		
		//# getAccentColor($('.bg-color img')[0]);
		// $('.project').each(function(i) {
		// 	getAccentColor($(this).find('img')[0], function(color) {
		// 		var project = $('.project').eq(i);
		// 		project.css('background-color','rgb('+color[2]+')');
		// 	});
		// });

		// $('.bg-color').each(function() {
		// 	getAccentColor($(this).find('img')[0], function(color) {
		// 		$('.bg-color').css('background-color','rgba('+color[2]+', 0.3)');
		// 		$('footer').css('background','rgb('+color[2]+')');
		// 	});
		// });

		initProjectView();

		

		if(isMobile) {
			$('.view-feature-projects .view-content').cycle({
				slides:'figure',
				fx:'scrollHorz',
				swipe:true,
				// carouselOffset:6,
				timeout:0
			});
			$('.view-feature-projects').on('cycle-post-initialize', function(e, optionHash, API) {
				API.initSlideshow = function() {
					initProjectView();
				}
			    // your event handler code here
			    // argument opts is the slideshow's option hash
			});
			$('.view-feature-projects .view-content').on('cycle-initialized', function() {
                initProjectView();
            }); 
		}

	});


	

  }
};


})(jQuery, Drupal, this, this.document);
