# The Lab ⚗️

Code examples, wips, etc.

## Index

- www -- Tailor'd live React site (<a href="http://tailord.design">http://tailord.design</a>)
- code-examples/drupal7 -- *drupal 7 themes*
- code-examples/villa -- *code samples from ruvilla.com*
- react-league-finder -- *learning react and built a tool to add 'leagues' and then find them in a search*