<?php
// var_dump($variables);
?>
<?php foreach ($rows as $id => $row): ?>
	<div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?><?php print ' data-cycle-hash="' . $view->result[$id]->nid . '"'; ?>>
		<?php print $row; ?>
	</div>
<?php endforeach; ?>