<?php
/**
 * @file
 * Returns the HTML for the page bottom region.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728140
 */
?>
<?php if ($content): ?>
  <div id="pg-btm" class="<?php print $classes; ?>">
    <!-- <a href="/" title="Home" rel="home" class="header__logo" id="logo"><img src="/logo.png" alt="Logo Image" class="header__logo-image" /></a> -->

    <div class="header__name-and-slogan" id="name-and-slogan">
      <h2 class="header__site-name" id="site-name">
        <a href="/" title="Home" class="header__site-link" rel="home"><span>Michael Rios.net</span></a>
      </h2>
      <!-- <div class="header__site-slogan" id="site-slogan">slogan</div> -->
    </div>

    <?php print $content; ?>
  </div>
<?php endif; ?>
