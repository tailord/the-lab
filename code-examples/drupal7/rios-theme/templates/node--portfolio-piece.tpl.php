<?php
// kpr($variables);
//http://drupalcontrib.org/api/drupal/drupal--modules--node--node.tpl.php
//node--[CONTENT TYPE].tpl.php
if ($classes) {
  $classes = ' class="clearfix '. $classes . ' "';
}

if ($id_node) {
  $id_node = ' id="'. $id_node . '"';
}

$crop_img = $content['field_photo']['#items'][0]['uri'];
$orig_img = substr_replace($crop_img, '_0.jpg', -4);


hide($content['comments']);
hide($content['links']);
?>
<!-- node.tpl.php -->
<article <?php print $id_node . $classes .  $attributes; ?> role="article">
  <div class="content">
    <figure>
      <?php //print render($content['field_photo']); ?>
      <!-- <img src="<?php //echo file_create_url($orig_img); ?>" srcset="<?php //echo file_create_url($crop_img); ?> 640w, <?php //echo file_create_url($orig_img); ?> 1600w" sizes="(max-width:640px) 320px, 1600px" /> -->
      <img data-img-srcset="<?php echo file_create_url($crop_img); ?> 640w, <?php echo file_create_url($orig_img); ?> 1600w" sizes="(max-width:640px) 320px, 1600px" />
      <figcaption>
        <h2><?php print $title; ?></h2>
        <?php print render($content['field_description']); ?>
        <?php print render($content['field_location']); ?>
        <?php print render($content['field_photo_date']); ?>
      </figcaption>
    </figure>
  </div>
</article>