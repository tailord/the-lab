/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {


	function ll(slide) {
		$('img', slide).each(function() {
			var src = $(this).data('img-srcset');
			$(this).attr('srcset', src);	
		});
	}


	$(document).ready(function() {
		if($('body.front')) {
			var _ran = false;
			if(!_ran) {
				$('.view-id-gallery').on('cycle-update-view', function (e, optionHash, slideOptionsHash, currSlideEl) {
				    ll(currSlideEl);
				});
				_ran = !_ran;
			}
			

			$('.view-id-gallery').on('cycle-before', function(event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag) {
				ll(incomingSlideEl);
			});
		}

		$('#i').click(function() {
			$('.view-id-gallery').toggleClass('open');
			if ($(this).html() == "i") {
				$(this).html('x');
			} else if ($(this).html() == "x") {
				$(this).html('i');
			}
		});

		// page click functions
		$('#gallery-icon').click(function() {
			$('#pg-btm').toggleClass('open');
		});
		$('.album-thumb').click(function() {
			if (!$('body').hasClass('front')) {
				var l = $(this).find('a').attr('href');
				window.location.href = "/" + l;
			}
			$(window).scrollTop();
			$('#pg-btm').toggleClass('open');
		});
	});


})(jQuery, Drupal, this, this.document);
