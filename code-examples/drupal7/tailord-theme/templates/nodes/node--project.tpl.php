<?php 
$title = $node->title;
$hero = $node->field_hero['und'][0]['uri'];
$overlay = $node->field_overlay_rgb['und'][0]['value'];
$caption = $node->field_hero_caption['und'][0]['value'];
$images = $node->field_project_image['und'];
$client = $node->field_client['und'][0]['value'];
$handoffs = $node->field_hand_offs['und'];
$link = $node->field_project_link['und'][0]['value'];
$prefix = '';
?>

<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <section class="hero opening" style="background-image:url(<?php echo file_create_url($hero) ?>);">
    <div class="title"><h2><?php echo $caption ?></h2></div>
    <div class="bg-color" style="background-color:rgba(<?php echo $overlay ?>, 0.3);">
      <img src="<?php echo file_create_url($hero) ?>" id="bg-img" alt="">
    </div>
  </section>
  <section class="project-view clearfix">
    <article class="clearfix">
      <h1 class="project-title"><?php print render($title) ?></h1>
      <h3 class="project-client"><?php echo $client ?></h3>      
      <?php if($link): ?><button class="button--small"><a href="<?php echo $link ?>" target="_blank">Visit Project</a></button><?php endif; ?>
      <?php print render($content['body']) ?>
      <p class="hand-offs"><?php foreach($handoffs as $handoff) {echo $prefix.$handoff['value'];$prefix = '<br>';} ?></p>
    </article>
    <div class="masonry clearfix">
      <?php foreach($images as $image): ?>
      <img src="<?php echo file_create_url($image['uri']) ?>" width="<?php echo $image['width'] ?>" alt="" class="brick">
      <?php endforeach; ?>
    </div>
    
  </section>

</div>

<?php 
//kpr($node);
?>
