<?php
kpr(get_defined_vars());
//http://drupalcontrib.org/api/drupal/drupal--modules--node--node.tpl.php
//node--[CONTENT TYPE].tpl.php
if ($classes) {
  $classes = ' class="clearfix '. $classes . ' "';
}

if ($nid) {
  $id_node = ' id="'. $nid . '"';
}
// $node = node_load($nid);
$hero = $content['field_post_hero_image']['#items'][0]['uri'];
$video = false; //just here for now to mute if/else statement
// $authpic = $node->user->picture;
$formatted_date = format_date($node->created, 'custom', 'n.j.y');
$node_author = user_load($node->uid);
$first_name = $node_author->field_user_name['und'][0]['given'];
$last_name = $node_author->field_user_name['und'][0]['family'];
$tax = taxonomy_term_load($node->field_blog_category['und'][0]['tid']);
$tax_logo = $tax->field_category_logo['und'][0]['uri'];
// $term_logo = field_view_field('taxonomy_term', $term, 'field_category_logo');
#kpr($node_author);
// print ($node_author->roles[3]);
// print ($node_author->field_biography['und'][0]['value']);

hide($content['comments']);
hide($content['field_post_hero_image']);
hide($content['field_blog_category']);
// hide($content['links']);

?>
<!-- node.tpl.php -->
<?php if($video) { ?>
	<?php echo theme('image', array(
	  'path' => file_create_url($content['field_blog_video'][0]['uri'])
	)); ?>
	<video>
		<source src="<?php echo drupal_realpath($content['field_blog_video'][0]['uri']); ?>">
		<source src="/sites/default/files/videos/<?php print $content['field_blog_video']['und'][0]['uri']; ?>">
	</video>
	<!-- print render($content['field_post_hero_image']); -->
<?php } else {
	echo '<figure class="hero" style="background-image:url('.file_create_url($hero).');">';
	//echo '<img src="'.file_create_url($hero).'" />';
	echo '<figcaption><h1>'.$title.'</h1></figcaption>';
	echo '</figure>';
} ?>
<article <?php print $id_node . $classes .  $attributes; ?> role="article">
	<header>
		<div class="auth-info">
			<?php print render($user_picture); ?>
			<?php //print $name; ?>
			<span class="auth-name"><?php print $first_name.' '.$last_name; ?></span>
			<span class="date">Posted on <?php print $formatted_date; ?></span>
		</div>
		<div class="blog-tax">
			<div class="blog-tax-logo">
				<img src="<?php print file_create_url($tax_logo); ?>" />
			</div>
			<?php print $tax->name; ?>
		</div>
	</header>
    <?php print render($content); ?>
</article>