<?php
#kpr(get_defined_vars());
/**
 * @file
 * Returns the HTML for a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728148
 */
?>

<div id="site-main">

  <main id="content" class="column" role="main">
    <?php //print render($page['highlighted']); ?>
    <?php //print $breadcrumb; ?>
    <!-- <a id="main-content"></a> -->
    <?php //print render($title_prefix); ?>
    <?php //print render($title_suffix); ?>
    <?php //print render($tabs); ?>
    <?php print render($page['help']); ?>
    <?php if ($action_links): ?>
      <ul class="action-links"><?php print render($action_links); ?></ul>
    <?php endif; ?>
    <?php print render($page['content']); ?>
    <?php //print $feed_icons; ?>
  </main>

  <?php if ($front_page): ?>
  <div class="project-card"></div>
  <?php endif; ?>

  
  <?php
    // Render the sidebars to see if there's anything in them.
    $sidebar_first  = render($page['sidebar_first']);
    $sidebar_second = render($page['sidebar_second']);
  ?>

  <?php if ($sidebar_first): ?>
    <aside class="sidebars sidebar--left">
      <?php print $sidebar_first; ?>
    </aside>
  <?php endif; ?>
  <?php if ($sidebar_second): ?>
    <aside class="sidebars sidebar--right">
      <?php print $sidebar_second; ?>
    </aside>
  <?php endif; ?>

  <?php print render($page['bottom']); ?>

  <header id="site-header" role="banner">
    <?php print $messages; ?>
    <?php print render($page['header']); ?>

    <?php if ($logo): ?>
      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
    <?php endif; ?>

    <?php if ($title && $is_front): ?>
      <?php if ($site_name || $site_slogan): ?>
        <div class="header__name-and-slogan" id="name-and-slogan">
          <?php if ($site_name): ?>
            <h1 class="header__site-name" id="site-name">
              <?php print $site_name; ?>
            </h1>
          <?php endif; ?>
          <?php if ($site_slogan): ?>
            <h4 class="header__site-slogan" id="site-slogan"><?php print $site_slogan; ?></h4>
          <?php endif; ?>
        </div>
      <?php endif; ?>
    <?php endif; ?>

    <?php /*if ($secondary_menu): ?>
      <nav class="header__secondary-menu" id="secondary-menu" role="navigation">
        <?php print theme('links__system_secondary_menu', array(
          'links' => $secondary_menu,
          'attributes' => array(
            'class' => array('links', 'inline', 'clearfix'),
          ),
          'heading' => array(
            'text' => $secondary_menu_heading,
            'level' => 'h2',
            'class' => array('element-invisible'),
          ),
        )); ?>
      </nav>
    <?php endif; */?>
    <?php print render($page['navigation']); ?>

  </header>

</div>

<?php print render($page['footer']); ?>
