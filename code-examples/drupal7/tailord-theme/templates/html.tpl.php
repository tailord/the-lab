<?php
/**
 * @file
 * Returns the HTML for the basic html structure of a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728208
 */
?><!DOCTYPE html>
<!--[if IEMobile 7]><html class="iem7" <?php print $html_attributes; ?>><![endif]-->
<!--[if lte IE 6]><html class="lt-ie9 lt-ie8 lt-ie7" <?php print $html_attributes; ?>><![endif]-->
<!--[if (IE 7)&(!IEMobile)]><html class="lt-ie9 lt-ie8" <?php print $html_attributes; ?>><![endif]-->
<!--[if IE 8]><html class="lt-ie9" <?php print $html_attributes; ?>><![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)]><!--><html <?php print $html_attributes . $rdf_namespaces; ?>><!--<![endif]-->

<head>
  <?php print $head; ?>
  <title><?php if ($is_front) {echo 'TAILOR\'D';} else {print $head_title;} ?></title>
  <meta name="description" content="Tailor'd Design is a creative agency. We develop and build brand strategies that elevate your story and make you look awesome." />
  <meta name="keywords" content="design, web, website, graphics, photography, wordpress, drupal, magento, cms, custom, tailored, build, code, css, html, javascript, philadelphia, philly" />

  <?php if ($default_mobile_metatags): ?>
    <meta name="MobileOptimized" content="width">
    <meta name="HandheldFriendly" content="true">
    <meta name="viewport" content="width=device-width">
  <?php endif; ?>

  <meta property="og:title" content="Tailor'd Design">
  <meta property="og:url" content="http://www.tailord.design" id="schema-url">
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@TailorDesign">
  <meta name="twitter:title" content="Tailor'd Design">
  <meta property="og:type" content="website"/>
  <meta property="og:image" content="http://tailord.design/sites/default/files/page_hero/flowervintage.jpg" />
  <meta property="og:description" content="Tailor'd Design is a creative agency. We develop and build brand strategies that elevate your story and make you look awesome." />
  <meta name="twitter:image" content="http://tailord.design/sites/default/files/page_hero/flowervintage.jpg">
  <meta name="twitter:description" content="Tailor'd Design is a creative agency. We develop and build brand strategies that elevate your story and make you look awesome.">
  <meta http-equiv="cleartype" content="on">
  <link rel="icon" href="/sites/all/themes/tailord/favicon.ico" type="image/x-icon">
  <link rel="shortcut icon" href="/sites/all/themes/tailord/favicon.ico" type="image/x-icon">
  <link rel="icon" type="image/png" sizes="16x16" href="/sites/all/themes/tailord/favicon-16x16.png">
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/sites/all/themes/tailord/apple-icon-144x144.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/sites/all/themes/tailord/apple-icon-114x114.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/sites/all/themes/tailord/apple-icon-72x72.png">
  <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/sites/all/themes/tailord/apple-icon-57x57.png">

  <?php print $styles; ?>
  <?php print $scripts; ?>
  <?php if ($add_html5_shim and !$add_respond_js): ?>
    <!--[if lt IE 9]>
    <script src="<?php print $base_path . $path_to_zen; ?>/js/html5.js"></script>
    <![endif]-->
  <?php elseif ($add_html5_shim and $add_respond_js): ?>
    <!--[if lt IE 9]>
    <script src="<?php print $base_path . $path_to_zen; ?>/js/html5-respond.js"></script>
    <![endif]-->
  <?php elseif ($add_respond_js): ?>
    <!--[if lt IE 9]>
    <script src="<?php print $base_path . $path_to_zen; ?>/js/respond.js"></script>
    <![endif]-->
  <?php endif; ?>
  <link href="https://fonts.googleapis.com/css?family=Mukta+Vaani:300|Poppins:700" rel="stylesheet">
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-44399457-2', 'auto');
    ga('send', 'pageview');

  </script>
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
  <?php /*if ($skip_link_text && $skip_link_anchor): ?>
    <p id="skip-link">
      <a href="#<?php print $skip_link_anchor; ?>" class="element-invisible element-focusable"><?php print $skip_link_text; ?></a>
    </p>
  <?php endif; */ ?>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
  <?php /*<footer id="site-footer">
    <div>&copy; Tailor'd Design 2013-2017. All Rights Reserved.</div>
  </footer> */ ?>
</body>
</html>
