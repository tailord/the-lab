<?php 
// kpr($row);
$nid = $row->nid;
$path = drupal_get_path_alias('node/'.$nid);
$hero = $row->field_field_hero[0]['raw']['uri'];
$overlay = $row->field_field_overlay_rgb[0]['raw']['value'];
$title = $row->node_title;
$client = $row->field_field_client[0]['raw']['value'];
// $handoffs = $row->field_field_hand_offs;
$prefix = '';
// hide($content['field_hero']);
?>

<?php /* ?><div data-load-nid="<?php echo $nid ?>" style="background-color:rgb(<?php echo $overlay ?>);">
	<img src="<?php echo file_create_url($hero) ?>" alt="">
	<div class="bg-img" style="background-image:url('<?php echo file_create_url($hero) ?>');"></div>
	<figcaption>
		<h3><?php print $client ?><span><?php print $title ?></span></h3>
		<p><?php foreach($handoffs as $handoff) {
	        echo $prefix.$handoff['raw']['value'];
	        $prefix = ', ';
	    } ?>
	  </p>
	</figcaption>
	<span class="icon icon--eye"></span>
</div>
<?php */ ?>
<div class="teaser2" data-load-nid="<?php echo $nid ?>" style="background-color:rgb(<?php echo $overlay ?>);">
	<img src="<?php echo file_create_url($hero) ?>" alt="">
	<figcaption>
		<h3><?php print $title ?></h3>
		<h4><?php print $client ?></h4>
		<span class="icon icon--eye"></span>
	</figcaption>
</div>