# Exercise 2
*required modules*

- Webform
- Views

### Hero block
Content Block

Request Form CTA

• Setup a Webform form with all fields listed

• Setup a View block using the Webform body of nid of previously made Webform

### Map block
Content Block

Tax Landscape

• Basic HTML block with img

### Remote Seller Nexus Rules
View Block

Nexus Rules

(Header section of Global: Text Area for intro)

Displays fields from custom content type

(Footer section of Global: Text Area for disclaimer)

• Setup a View block as Table using the Nexus Tax content type below, filtered by published and sorted by State descending. Add a field for custom text used for the read more text. Make a custom view template sticking the Body row under each content row, hidden. Set a js toggle against the read more text to show/hide the Body text (and change the 'read more' text).

### Nexus Tax Content Type
Fields:

__State__

- Text field
- Required
- Custom field

__Effective Date__

- Date / text (Date | In Review | Proposed | N/A)
- Required
- Custom field

__Threshold Trigger__

- Text field
- Custom field

__Transaction for Threshold__

- Option list (Retail sales | Sales | N/A)
- Custom field

__Body__

- Required
- Default field