## Details

All files here are used for Magento 1.x templating

- list.phtml is category view
- view.phtml is product view
- releases.phtml is a special layout for upcoming product using list.js
- local.xml is a layout file for templating