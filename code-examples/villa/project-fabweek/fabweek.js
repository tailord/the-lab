// 
//   ___ ___ ___ ___     ___     _______ 
//  |   V   |   |   |   |   |   |       |
//  |.  |   |.  |.  |   |.  |   |.  -   |
//  |.  |   |.  |.  |   |.  |   |.      |
//  |:      |:  |:  |___|:  |___|:  |   |
//   \:.. ./|::.|::.. . |::.. . |::.|:. |
//    `---' `---`-------`-------`--- ---'
// 


//--------------------------------------//
// Fabweek js
//--------------------------------------//


(function($) {

	var isMobile = (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent));

	function navBtn() {
		var p = $(this).data('product-find');
		
		//fallback incase image is being clicked instead of figure
		if($(this).is('img')) {
			p = $(this).parent('figure').data('product-find');
		}

		$t = $('.story[data-product="'+p+'"]');
		$details = $t.find('.product-details');
		// console.log(p);

		if ($t.hasClass('active') && _on) {
			return false;
		}
		
		if($t.is('[data-hide]')) {
			$t.show();
		}

		$('.story').removeClass('active');

		details($details, _on, $t);
		$('[data-product-find]').off('click');

		if(!_on) {
			swap($t);
			$t.addClass('active');			

			setTimeout(function() {
				$('.x').addClass('slide');
				if (($(window).scrollTop() != 0) && _on) {
					st();
					$('#block--heroes, #block--products').addClass('modal');
				} else {
					$('#block--heroes, #block--products').addClass('modal');
				}
			}, 900);
			_on = true;

		} else {
			st();
			shuffle($t);
		}
	}

	function shuffle(t) {
		t.addClass('shuffle shuffle-effect active');
		setTimeout(function() {
			t.removeClass('shuffle-effect');
			setTimeout(function() {
				swap(t);
			}, 1800);
		}, 300);
	}

	function swap(t) {
		t.insertAfter($('#block--products > .nav--products')).removeClass('shuffle');
		t.siblings('[data-hide]').hide();
		$('[data-product-find]').on('click', navBtn);
	}

	function details(d, _on, t) {
		var timer = 1300;
		d.removeClass('shuffle');
		if(!_on) {
			timer = 700;
		}
		setTimeout(function(){
			d.addClass('shuffle');
			livephoto(t);
		}, timer);

		// setTimeout(function() {
		// 	hookupToggle(d.siblings('.desktop-slider').find('.icon--hookup'));
		// }, 400);
	}

	function st() {
		$('html, body').animate({scrollTop:0}, 900, 'swing', function() {
			$('#fabweek').addClass('fixed');

			setTimeout(function() {
				$('#fabweek').removeClass('fixed');
			}, 900);
		});
	}

	function hookupToggle(h) {
		var $hookups = h.parent().siblings('.product-hookups');
		$hookups.find('.story-items').slideToggle(450);
		$hookups.find('.story-items ul > li').each(function(i) {
			var $item = $(this);
			setTimeout(function() {
				$item.toggleClass('animate');
			}, 90*i);
		});
	}

	function _countdown() {
		if ($('.product-release')[0]) {
			$('.product-release').each(function() {
				var date = $(this).data('release-time');
				$(this).find('span').countdown(date)
				.on('update.countdown', function(event) {
					$(this).html(event.strftime('%D:%H:%M:%S'));
					return false;
				})
				.on('finish.countdown', function(event) {
					$(this).parent().addClass('hide');
					$(this).parents('.product-atc').find('.icon--atc').addClass('show');
				});
			});
		}
	}

	function livephoto(t) {
		var img = t.find('.desktop-slider figure[data-live-photo]');
		var product = t.data('product');
		var m = '';
		if (isMobile) {m = '-mobile';}

		if(img) {
			var counter = 1;
			var speed = 60;
			var frames = 16;

			var swapper = setInterval(function() {
				img.css({'background-image':'url(https://www.dtlr.com/media/stories/fabolous-week/live-photos/'+product+'-'+counter+m+'.jpg?)'});
				counter++;
				if (counter > frames) {
					clearInterval(swapper);
					//reverse
					// var reppaws = setInterval(function() {
					// 	img.css({'background-image':'url(https://www.dtlr.com/media/stories/fabolous-week/live-photos/'+product+'-'+counter+m+'.jpg?)'});
					// 	counter--;
					// 	if (counter < 1) {clearInterval(reppaws);}
					// }, speed);
				}
			}, speed);
		}
	}

	function _videoPlayer() {
		$('video').on('click', function() {
			this.paused?this.play():this.pause();
		});
		if(!isMobile) {
			$('.story video').on('play', function() {
				$(this).parents('.story').find('.product-details').removeClass('shuffle');
			});
			$('.story video').on('pause', function() {
				$(this).parents('.story').find('.product-details').addClass('shuffle');
			});
		}
	}


	$(document).ready(function() {

		//////////////////////////////
		//CLICKS
		// $('button').on('click', livephoto);
		// $('[data-product="question"] .product-brand').on('click', livephoto);

		_videoPlayer();


		//////////////////////////////
		//RELEASE CLOCKS
		_countdown();


		//////////////////////////////
		//URL REDIRECTS
		var products = ['question','timberland','new-balance','foamposite','converse','adidas','retro3','retro1','reebok','starter-grid','starter-golden-state','starter-nba-west','starter-lakers','starter-76ers','starter-michigan','starter-cavaliers'];

	    for (var i = 0; i < products.length; i++) {
	        if(window.location.href.indexOf(products[i]) > -1) {		
			    $t = $('.story[data-product="'+products[i]+'"]');
				swap($t);
				$t.addClass('active');
				$('html, body').animate({scrollTop:0}, 900, 'swing');


				setTimeout(function() {
					$('#block--heroes, #block--products').addClass('modal');
					_on = true;
				}, 1200);
			}
		}

		//////////////////////////////
		//SLIDER PAGERS
		$.each($('.pager'), function() {
			var $slider = $(this).siblings('.desktop-slider');
			var	m = '';
			if (isMobile) {
				$slider = $(this).siblings('.mobile-slider');
				m = '-mobile';
			}

			var product = $slider.parent().data('product');
			var	anchor = 0;

			$(this).find('> div').on('click', function(e) {
				var i = $(this).index();

				if(i != 0) {
					if($(this).data('paged') === false) {
						var s = i+1;
						// console.log(s+','+m);
						if(i == anchor+1) {
							if ($(this).data('is-video') === true) {
								$slider.cycle('add', '<figure style="background:#2b2b2b;"><video width="auto" controls="" preload="none" poster="https://www.dtlr.com/media/stories/fabolous-week/'+product+'/'+product+'-video-poster.jpg"><source src="https://www.dtlr.com/media/stories/fabolous-week/'+product+'/'+product+'-video.mp4" type="video/mp4"></video></figure>');
								_videoPlayer();
							} else {
								$slider.cycle('add', '<figure style="background-image:url(\'https://www.dtlr.com/media/stories/fabolous-week/'+product+'/'+product+'-'+s+m+'.jpg\');"></figure>');
							}
							$(this).data('paged', true);
						} else {
							var j = 0;
							while (j < (i - anchor)) {
								s = anchor+2+j;
								if (($(this).data('is-video') === true) && (s-1 == i) ) {
									$slider.cycle('add', '<figure style="background:#2b2b2b;"><video width="auto" controls="" preload="none" poster="https://www.dtlr.com/media/stories/fabolous-week/'+product+'/'+product+'-video-poster.jpg"><source src="https://www.dtlr.com/media/stories/fabolous-week/'+product+'/'+product+'-video.mp4" type="video/mp4"></video></figure>');
									_videoPlayer();
								} else {
									$slider.cycle('add', '<figure style="background-image:url(\'https://www.dtlr.com/media/stories/fabolous-week/'+product+'/'+product+'-'+s+m+'.jpg\');"></figure>');
								}
								$(this).parent().find('> div:eq('+j+')').data('paged', true);
								j++;
							}
							$(this).data('paged', true);
						}
					}
				}
				$(this).addClass('active').siblings().removeClass('active');
				$slider.cycle('goto', i);
				anchor = i;

				if(i == 0) {
					var $t = $slider.parent();
					e.stopImmediatePropagation();
					livephoto($t);
				}

				if(!$(this).attr('data-is-video')) {
					if($(this).siblings().attr('data-is-video')) {
						$slider.find('video').get(0).pause();	
					}
				}
			});
		});


		//////////////////////////////
		//CARDS
		var $finder = $('[data-product-find], [data-product-find] img');
				_on = false;

		$finder.on('click', navBtn);


		//////////////////////////////
		//HOOKUPS & SCROLL
		if(!isMobile) {
			$('.icon--hookup').on('click', function() {
				hookupToggle($(this));
				$(this).data('lock', true);
			});

			$(window).scroll(function() {
				var g = $(window).height() / 1.8;
				var h = $(window).height() / 2;
				$('.icon--hookup').each(function() {
					var t = $(this).offset().top - h;
					var a = (window.pageYOffset > t);
					if(!$(this).data('lock') === true) {
						if (a) {
							hookupToggle($(this));
							$(this).data('lock', true);
						}
					}
				});
				$('[data-live-photo]').each(function() {
					var t = $(this).offset().top - g;
					var b = (window.pageYOffset > t);
					if(b && $(this).data('lock') === false) {
						livephoto($(this).parents('.story'));
						$(this).data('lock', true);
					}
				});
			});
		}


		//////////////////////////////
		//X
		$('#block--products .x').on('click', function() {
			_on = false;
			$(this).removeClass('slide');
			$('#block--heroes, #block--products').removeClass('modal');
		});


		//////////////////////////////
		//SLIDERS
		if(isMobile) {
			$('.mobile-slider').cycle({
				slides:"figure",
				timeout:0,
				manualSpeed:800,
				sync:true,
				swipe:true
			});
			$('.mobile-heroes').cycle({
				slides:"figure",
				pauseOnHover:true,
				swipe:true
			});
			$('.nav--products > div').cycle({
				carouselVisible:2,
				allowWrap: false,
				slides:"div.nav--product",
				fx:"carousel",
				prev:".nav--products .cycle-prev",
				next:".nav--products .cycle-next",
				pauseOnHover:true,
				swipe:true
			});
		} else {
			$('.desktop-slider').cycle({
				slides:"figure",
				timeout:0,
				manualSpeed:800,
				sync:true
			});
			$('.desktop-heroes').cycle({
				slides:"figure",
				pauseOnHover:true
			});
			$('.nav--products > div').cycle({
				carouselVisible:4,
				allowWrap: false,
				slides:"div.nav--product",
				fx:"carousel",
				prev:".nav--products .cycle-prev",
				next:".nav--products .cycle-next",
				pauseOnHover:true
			});
		}

	});


})(jQuery);