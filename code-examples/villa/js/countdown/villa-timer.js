jQuery(document).ready(function($) {
	var recache = function() {
		var timestamp = Math.floor(Date.now() / 1000);
		if (release < timestamp) {
			$.ajax({
				type: "POST",
				dataType: 'json',
				url: '/service/ajax/product_clean.php',
				data: $vtimer.data(),
				success: function(ajax) {
					if (ajax.refresh == true) {
						clearInterval(countdown_interval);
						document.location.reload(true);
					}
				},
				error: function(jqXHR, strStatus, strError) { 
					/* strStatus can be timeout, error, abort, parseerror 
					alert('Server Error encountered while attempting to clear cache.'+"\n"+
						'Status: '+strStatus+"\nError: "+strError+"\nXHR: "+jqXHR);*/
					return;
				}
			});
		}
	}

	var $vtimer = $('.rel-date');
	if ($vtimer.length > 0) { // if page contains release date info
		var release = parseInt($vtimer.data('release'));
		recache();
		// recheck release every 5 seconds
		var countdown_interval = setInterval(recache, 5000);
	}
});