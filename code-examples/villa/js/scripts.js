// 
//   ___ ___ ___ ___     ___     _______ 
//  |   V   |   |   |   |   |   |       |
//  |.  |   |.  |.  |   |.  |   |.  -   |
//  |.  |   |.  |.  |   |.  |   |.      |
//  |:      |:  |:  |___|:  |___|:  |   |
//   \:.. ./|::.|::.. . |::.. . |::.|:. |
//    `---' `---`-------`-------`--- ---'
// 


//--------------------------------------//
// VILLA js
//--------------------------------------//


(function($) {

	var _ipad = (/iPad/i.test(navigator.userAgent));
		breakpoint = 760;
	$.fn.cycle.log = $.noop

	function _ieDetect() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");
        var trident = ua.indexOf('Trident/');
        var edge = ua.indexOf('Edge/');

	    if (msie > 0 || trident > 0 || edge > 0) {
	    	$('html').addClass('ie');
	    }
	}

	function _lazyLoad() {
		var ias = $.ias({
			container:  '.catalog',
			item:       '.product',
			// pagination: '#filters .pages ol',
			next: 		'#next-page',
		  	// triggerPageThreshold : 9999,
			// negativeMargin: 700
			// onRenderedComplete: _pwrfeed(items)
			// loaded: _countdown()
		});

		ias.on('rendered', function(items) {
			_countdown();
			_pwrfeed(items);
		});
	    ias.extension(new IASTriggerExtension({
	    	offset:  	3,
		    text: 		'<button class="dark">Show More</button>',
		    textPrev: 	'<button class="dark">Show Previous</button>'
		}));

	    //needed for going back in browser and retaining page position
		ias.extension(new IASPagingExtension());
		ias.extension(new IASHistoryExtension({
			prev: 		'#prev-page'
		}));

		ias.extension(new IASSpinnerExtension({
			html: 		'<div class="icon icon--notch"></div>'
		})); 
		ias.extension(new IASNoneLeftExtension({
		  text: 		'No more results.' //optionally
		}));
	}

	function _pwrfeed(items) {
		var $items = $(items);
		var locale = 'en_US';
	    var api_key = 'f296220b-cd2f-4448-a7c3-7c6e738c4249';
	    var merchant_group_id = '77641';
	    var merchant_id = '430665';
	    var pwrFeedRender = new Array();
	    // console.log('%c'+$items,'color:blue;');
		$items.each(function() {
		    // console.log("%c"+$(this),'color:orange;');
			pwrFeedRender.push({
				locale: locale,
	            merchant_group_id: merchant_group_id,
	            page_id: $(this).data('product-style'),
	            merchant_id: merchant_id,
	            api_key: api_key,
	            review_wrapper_url: '/submit-a-review?pr_page_id='+$(this).data('product-style'),
	            components: {CategorySnippet: 'review-'+$(this).data('product-id')}
			});
		});

		POWERREVIEWS.display.render(pwrFeedRender);
	}

	function _lazyLoadOrders() {
		var ias = $.ias({
			container:  '.account-orders',
			item:       '.row-order',
			// pagination: '#pagination',
			next:       'a#next-page',
		  	triggerPageThreshold : 9999,
			negativeMargin: 500
		});

	    ias.extension(new IASTriggerExtension({
	    	offset:3,
		    text: '<button class="dark">Show More</button>'
		}));
		ias.extension(new IASSpinnerExtension());
	}

	function toTop() {
		$('html, body').animate({scrollTop:0}, 500);
	}

	function _countdown() {
		if ($('.product-releasetime')[0]) {
			var now = new Date($.now());
			var nowEST = new Date(moment.tz(now, "America/New_York").format('LLL'));

			var timeZoneDiff = ((now.getTime() - nowEST.getTime()) / 1000) / (60 * 60);
			var timeAdjust = Math.abs(Math.round(timeZoneDiff)) 

			$('.product-releasetime').each(function() {
				var relDate = new Date($(this).html());
				relDate.setHours(relDate.getHours() - timeAdjust);
				
				if(relDate > now) {
					$(this).siblings('.product-releasetimer').find('span').countdown(relDate, function (event) {
						var d = event.offset.totalDays;
						h = event.offset.hours;
						m = event.offset.minutes;
						s = event.offset.seconds;
						//put this in place to combat html from caching .product-releasetime block
						if((d+h+m+s) == 0) {
							if($('body').hasClass('catalog-product-view')) {
			            		$('.product-release-box').fadeOut(500, function() {
				            		$('.product-shop').fadeIn(500);
			            		});
			            	} else {
			            		$(this).parents('.product-releasetimer').html('Now Available').find('.product-releasetime').hide();
			            	}
						} else {
							$(this).html(event.strftime('%D:%H:%M:%S'));
							if($('body').hasClass('catalog-product-view')) {
								$('.product-shop').hide();
								// $('.product-release-box').show();
							}
						}
		                return false; //necessary to populate countdown text
		            });
				} else {
					$(this).parent().hide();
					if($('body').hasClass('catalog-product-view')) {
						$(this).parents('.product-release-box').hide();
					}
				}
			});
		}
	}

	function popupFinder() {
		var t = $(this).data('popup-find');
		$('[data-popup="'+t+'"]').show();
	}

	function loadingPage() {
		$('#page-loading').show();
	}

	function initSliderWidget(obj) {
		var c = obj.container;
			fx = obj.fx || 'carousel';
			g = obj.group;
			gs = obj.groupSize || 4;
			offset = obj.offset || 0;
			wrap = obj.wrap || false;
			s = obj.slides || '.product';
			items = c.find(s);
		if (g) {
			if ($(window).width() < 1050) {gs -= 1;}
			if (items.length <= gs) {
				c.find('[class^=cycle-]').hide();
				return false;
			}
			s = groupWidgetSlides(c, gs, items);
		}
		c.cycle({
			slides:s,
			fx:fx,
			carouselOffset:offset,
			allowWrap:wrap,
			timeout:0,
			swipe:true
		});
	}

	function groupWidgetSlides(container, groupSize, items) {
		//wraps groups of slides with .group and changes the slide target as class '.group'
		for(var i = 0; i < items.length; i+=groupSize) {
	     	items.slice(i, i+groupSize).wrapAll("<div class='group'></div>");
	    }
	    return '.group';
	}

	$(document).ready(function() {

		if (navigator.userAgent.search("Firefox")) {
			$('body').addClass('firefox');
		}
		_ieDetect();
		_countdown();
		checkCartQty();
		$('body').addClass('js');

		// $('video').on('click', function() {
		// 	if (this.paused) {this.play();}
		// 	else {this.pause();}
		// });

		//popups
		$('[data-popup-find]').on('click', popupFinder);
		$('.popup').on('click', function(e) {
			if (e.target !== this) {return;}
			$(this).hide();
		});

		$('#btt').on('click', toTop);

		$('.icon--x').on('click', function() {
			$(this).parent().hide();
		});
		$('ul.messages').on('click', function() {
			$(this).hide();
		});

		$('.icon--filters').on('click', function() {
			$('#filters').slideToggle();
		});

		$('.product-blog article').on('click', function() {
			$(this).toggleClass('open').find('.post').slideToggle();
		});

		$('.outfit .icon-more').on('click', function() {
			$(this).toggleClass('open').parent().toggleClass('open');
		});

		if ('[data-move-username]') {
			$('[data-move-username]').prependTo('.header-top-user-menu ul.user-menu');
		}

		//checkout page
		if ($('body').hasClass('checkout-cart-index')) {
			$('#discount-coupon-form, #giftcard-form').find('input').keypress(function() {
				$(this).closest('form').find('button').addClass('dark');
				$(this).focusout(function() {
					if(!$(this).val()) {
						$(this).closest('form').find('button').removeClass('dark');
					}
				});
			});
		}

		//search
		$('.icon--search').on('click', function(e) {
			if (e.target !== this) {
			    return;
			}
			$('#body-main, #body-footer').toggleClass('wash');
			$(this).toggleClass('open').siblings('form').slideToggle();
			$('input#search').focus();

			//close nav
			$('html, body').removeClass('fixed');
			$('.icon--nav-anim').removeClass('open');
			if (($(window).width() < breakpoint) || _ipad) {
				$('.header-bottom').slideUp();
			}

			//hide search on body click
			$('.wash').on('click', function() {
				$('#body-main, #body-footer').removeClass('wash');
				$('.icon--search').removeClass('open').siblings('form').slideUp();
			});
		});


		//Qty functions
		var $updateBtn = $('button[name="update_cart_action"]');
			$checkOut = $('ul.checkout-types');
			qtyArray = $('input.qty').map(function() {
				return $(this).val();
			}).get().join();
			msgFlag = false;

		$('.qty-adjustment').on('click', function() {
			if($(this).is('#qty-less')) {
				$(this).siblings('input').val(function(i, v) {
					if ($('body').hasClass('checkout-cart-index')) {
						if (v > 0) {
							return --v;
						} else {
							alert("Can Not Choose Less than 0!");
							return v;
						}
					} else {
						if (v > 1) {
							return --v;
						} else {
							alert("Can Not Choose Less than 1!");
							return v;
						}
					}
			    });
			} else {
				$(this).siblings('input').val( function(i, v) {
					// if (v < 10) {
						return ++v;
					// } else {
					// 	alert("Max Quantity Allowed. Please contact 1-844-RUVILLA for greater quantities.");
					// 	return v;
					// }
			    });
			}

			if ($('body').hasClass('checkout-cart-index')) {
				var qtyArrayNew = $('input.qty').map(function() {
					return $(this).val();
				}).get().join();
				if (qtyArrayNew != qtyArray) {
					$updateBtn.show();
					$checkOut.on('mouseenter',function() {
						if(!msgFlag) {
							alert('Proceeding to checkout will remove the recent changes in your cart! Please verify your cart looks correct before continuing.');
							msgFlag = true;
						}
					});
				} else {
					$updateBtn.hide();
				}
			}
		});

		//catalog js
		if ($('body').hasClass('catalog-category-view') || $('body').hasClass('catalogsearch-result-index')) {
			if ($('section.catalog')[0]) {
				_lazyLoad();
				_pwrfeed($('.catalog .product').toArray());
			}
		}
		//manually move this due to templating conflict
	    if ($('.toolbar-li.currently')[0]) {
	        $('.toolbar-li.currently').appendTo('.toolbar');
	    }
		$('.filters select').each(function () {
            $(this).val($(this).find('option[selected]').val());
        });
        // $('.toolbar-li.currently a').on('click', function() {
        // 	loadingPage();
        // });

        //Ensure the last top level category has class Last to ensure static links are added to Nav.
        $('.header-bottom li.level-top').last().addClass('last');

		//move static links into nav
		$('.header-bottom li[data-move-to-nav]').insertAfter($('.header-bottom li.level-top').last());

		//form adjustments
		$('label').each(function() {
			$(this).appendTo($(this).siblings('.input-box'));
		});

		$('.input-box input, .input-box textarea').each(function() {
			//var label = $(this).siblings('label').text();
			$(this).attr('placeholder','');
			if($(this).val()) {
				$(this).parent().addClass('in-use');
			}
		}).focus(function() {
			$(this).parent().addClass('in-use');
		}).focusout(function() {
			if(!$(this).val()) {
				//var label = $(this).siblings('label').text();
				$(this).parent().removeClass('in-use');
			}
		});

		$('.input-box select').each(function() {
			$(this).parent().addClass('in-use');
		});

		//checkout js
		$('#checkout-step-login button[type="submit"], #onepage-guest-register-button').hide();
		$('#login-form input').focus(function() {$('#checkout-step-login button[type="submit"]').show();});
		$('#checkout-step-login .control input').click(function() {$('#onepage-guest-register-button').show();});
        $('.address-sameasbilling input').on('click', function() {
            $('#shipping-new-address-form .input-box').addClass('in-use');
        });
        $('#opc-shipping .step-title').on('click', function() {
            $('#shipping-new-address-form .input-box').addClass('in-use');
        });
		$('.ship-readmore').on('click', function() {
			$(this).find('div').toggle();
		});
		var $updateShippingBtn = $('#update_shipping_method_submit');
		if ($('body').hasClass('paypal-express-review')) {
			var $activeShip = $('#shipping_method_form .method input:checked').attr('id');
			$('#shipping_method_form .method label').on('click', function() {
				// console.log($(this).siblings('input').attr('id'));
				if($(this).prop('checked', false)) {$updateShippingBtn.show();}
				if ($(this).siblings('input').attr('id') == $activeShip) {$updateShippingBtn.hide();}
			});
		}


		//account js
		if ($('body').is('.sales-order-history, .enterprise-rma-return-history')) {
			_lazyLoadOrders();
		}

		//giftcard js
		if ($('body').hasClass('product-egift-card')) {
			var amountRadio = $('input[type="radio"]');
				amountText = $('#giftcard_amount_input');
	        $(amountText).focusout(function() {
	            if($(this).val()) {
	            	$(this).addClass('giftcard-min-amount giftcard-max-amount');
                    $(amountRadio).each(function() {
                    	$(this).prop('checked', false);
                    });
	            }
	        });
	        $(amountRadio).on('click', function() {
	            if($(this).is(':checked')) {
	            	$(amountText).val('').removeClass('giftcard-min-amount giftcard-max-amount');
	            }
	        });
		}

		//coupon page
		// if ($('body').hasClass('cms-villa-coupon-codes')) {
		// 	$('.coupon h2').on('click', function() {
		// 		$(this).select();
		// 		document.execCommand('copy');
		// 		$(this).addClass('copied')
		// 	});
		// }

		//info popup
		$('[data-popup-find]').on('click', function() {
		    var t = $(this).data('popup-find');
		    $('[data-popup="'+t+'"]').show();
		    $('.popup').on('click', function() {$(this).hide();});
		});


		///////////////adjustments for desktop
		if (($(window).width() > breakpoint) && !_ipad) {
			var $si = $('[data-shift-icons]');
				$htt = $('.header-top-tools');

			$('body').addClass('view--desktop');

			$htt.prepend($('.header-top-user')).append($('.header-top-locator')).append($('.header-top-blog'));
			$si.append($('.header-top-search'));
			$('.header-top-promo').prependTo('.header-top');
			$('li.nav-5 ul.level0').prepend('<li class="level1 menu-title first parent"><a href="#"><span>Featured</span></a></li>');
			$('li.nav-5 ul.level0').append('<li class="level1 last parent"><a href="/brands.html">View All</a></li>');



			//cart rollover 
			$('.header-top-cart').hover(function() {
				$('body').addClass('fixed');
				$('#body-main, #body-footer').addClass('wash');
			}, function() {
				if (!$('.header-top-tools .icon--nav-anim').hasClass('open')) {
					$('body').removeClass('fixed');	
				}
				if (!$('.header-top-search .icon--search').hasClass('open')) {
					$('#body-main, #body-footer').removeClass('wash');
				}
			});


			$(window).scroll(function(){
				a = (window.pageYOffset > 25);
				b = (window.pageYOffset > 56);
				// c = (window.pageYOffset > 150);

				// $('.header-logo .logo').toggleClass('scroll', a);
			    $('.header-bottom .logo').toggleClass('snap', b);
			    $('#body-header, .header-bottom').toggleClass('snap', b);
			    $si.toggleClass('shift', b);
			    if (b) {$htt.appendTo($si);}
			    else {$htt.appendTo('.header-top');}
			});

			//user rollover
			$('.header-top-user').hover(function() {
				$('body').addClass('fixed');
			}, function() {
				$('body').removeClass('fixed');
			});

			//highlight list and shop the look sliders
			$('[data-cyclethis]').each(function() {
				initSliderWidget({
					container:$(this),
					fx:'scrollHorz',
					group:true,
					wrap:true
				});
			});
			initSliderWidget({
				container:$('.outfit-list-wrap'),
				fx:'scrollHorz',
				group:true,
				groupSize:3,
				wrap:true,
				slides:'.outfit'
			});
		}

		
		//////////////adjustments for mobile
		if (($(window).width() < breakpoint) || _ipad) {

			// responsive nav
			_lock = false;
			ss = 0;
			t = 400;
			$h = $('#body-header');

			if ($('body.catalog-product-view #product-addtocart-button')[0]) {
				var $atcBtn = $('#product-addtocart-button');
				// atcTop = $atcBtn.offset().top;
			}
			var _atclock = false;

			//pdp
			if ($('body').hasClass('catalog-product-view')) {
				//move slider to top due to template layout for SEO
				$('.product-details-left').prependTo($('.product-details'));
			}

			//cart rollover
			var cartToggle = false;
			$('.header-top-cart > a').on('click', function(e) {
				e.preventDefault();
			});
			$('.header-top-cart').on('click', function() {
				if (!cartToggle) {
					$('.header-top-cart').addClass('hover');
					$('body').addClass('fixed');
					$('#body-main, #body-footer').addClass('wash');
				} else {
					$('.header-top-cart').removeClass('hover');
					if (!$('.header-top-tools .icon--nav-anim').hasClass('open')) {
						$('body').removeClass('fixed');	
					}
					if (!$('.header-top-search .icon--search').hasClass('open')) {
						$('#body-main, #body-footer').removeClass('wash');
					}
				}
				cartToggle = !cartToggle;
			});
			
			$(window).scroll(function() {
				var a = (window.pageYOffset > t);
				st = $(this).scrollTop();

				if(a) {
					if(!_lock) {
						if(st < ss) {
							$h.css({top:'-'+t+'px',position:'fixed'}).animate({top:0},550);
							_lock = !_lock;
						}
						ss = st;
					}
				} else {
					if(_lock) {
						$h.animate({top:'-'+t+'px'}, 400, function() {
							$h.css({position:'absolute',top:0});
						});
						_lock = !_lock;
						ss = 0;
					}
				}

				if (!_ipad) {
					if($('body.catalog-product-view #product-addtocart-button')[0]) {
						if(!$('body').hasClass('product-egift-card')) {
							var atcScroll = (window.pageYOffset > ($(window).height()*1.5));
							if (atcScroll) {
								if (!_atclock) {
									$atcBtn.css({bottom:'-50px',position:'fixed'}).animate({bottom:0}, 300);
									_atclock = !_atclock;
								}
							} else {
								if (_atclock) {
									$atcBtn.animate({bottom:'0px'}, 300, function() {
										$atcBtn.css({position:'relative'});
									});
									_atclock = false;
								}
							}
						}
					}
				}				
			});

			//menu
			$('#body-nav .level0.parent > a, #body-nav .level1.parent > a, .header-top-user > a').on('click', function(e) {
				e.preventDefault();
				$(this).toggleClass('open').siblings('ul, div').slideToggle();
			});

			$('.icon--nav-anim').on('click', function() {
				$(this).toggleClass('open');
				$('.header-bottom').scrollTop(0).slideToggle();
				$('html, body').toggleClass('fixed');
			});

			//add in accessibility links
			$('li.parent > a').each(function() {
				var link = $(this).attr('href');
				if ($(this).parent().hasClass('level-top')) {
					$(this).siblings('ul').prepend('<li class="level1"><a href="'+link+'">Featured</a></li>');
				} else if ($(this).parent().hasClass('level1')) {
					$(this).siblings('ul').append('<li class="level2"><a href="'+link+'">View All</a></li>');
				}
			});


			$('.brand-header').on('click', function() {
				$(this).toggleClass('open').find('.brand-nav').slideToggle(250);
			});

			$('.header-top-user.parent > a').append($('ul.user-menu > h4')); //just bc of way template is
			$('.header-top-user-menu').prepend($(this).find('.top-user-menu-checkout')); // ""

			$('.footer-links-nav ul').on('click', function() {
				$(this).toggleClass('open');
			});

			//assets
			$('.hero .feature').each(function() {
				if ($(this).find('.asset-icons').children().length == 0) {
					$(this).find('.icon--circle').hide();
				}
			});
			// if (!$('[data-product-id] > a .asset')[0]) {
			// 	console.log('hey <?php echo $id ?>');
			//     $('[data-product-id="<?php echo $id ?>"] .icon--circle').addClass('what').hide();
			// }

			$('.product-assets .icon-more').on('click', function() {
				$(this).toggleClass('open').siblings('a').find('.asset-icons').toggleClass('open');
			});

			//mobile only
			if (!_ipad) {
				$('[data-cyclethis]').each(function() {
					initSliderWidget({
						container:$(this),
						offset:67
					});
				});
				initSliderWidget({
					container:$('.outfit-list-wrap'),
					slides:'.outfit'
				});
			}
		}

		if (($(window).width() < breakpoint) && !_ipad) {
			

			// var $atcBtn = $('#product-addtocart-button');
			// _lock2 = false;
			// // ss = 0;
			// t = $atcBtn.offset().top;
			// // w = $(window).height();

			// $(window).scroll(function() {
			// 	a = (window.pageYOffset > ((t+50) - $(window).height()));
			// 	// console.log(total);
			// 	// st = $(this).scrollTop();
			// 	if (a) {
			// 		if (!_lock2) {
			// 			$atcBtn.css({bottom:'-50px',position:'fixed'}).animate({bottom:0}, 300);
			// 			_lock2 = !_lock2;
			// 		}
			// 	} else {
			// 		if (_lock2) {
			// 			$atcBtn.animate({bottom:'-50px'}, 300, function() {
			// 				$atcBtn.css({position:'relative'});
			// 			});
			// 			_lock2 = false;
			// 		}
			// 	}
			// });
		}


	});

	$(document).on('recently-viewed-rerender', function(e) {
		$('[data-cyclethis]').each(function() {
			initSliderWidget({
				container:$(this),
				fx:'scrollHorz',
				group:true,
				wrap:true
			});
		});
		if (!_ipad) {
			$('[data-cyclethis]').each(function() {
				initSliderWidget({
					container:$(this),
					offset:67
				});
			});
		}
	});


})(jQuery);