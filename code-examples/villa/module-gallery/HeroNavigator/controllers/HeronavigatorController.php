<?php
/**
 * Villa Hero Navigator front end
 *
 * @category    Villa
 * @package     Villa_HeroNavigator
 * @author      Villa <jesse.crowell@ruvilla.com>
 */
class Villa_HeroNavigator_HeronavigatorController extends Mage_Adminhtml_Controller_Action {
    
    public function indexAction() {
        $this->loadLayout();          
        $this->getLayout()->getBlock('root')->setTemplate('villa/heronavigator/page.phtml');
        $this->renderLayout();
    }

    protected function _isAllowed() {
		return Mage::getSingleton('admin/session')->isAllowed('villa_core/villa_heronavigator');
	}

}