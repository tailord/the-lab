<?php
/**
 * Villa Hero Navigator front end
 *
 * @category    Villa
 * @package     Villa_HeroNavigator
 * @author      Villa <jesse.crowell@ruvilla.com>
 */
class Villa_HeroNavigator_IndexController extends Mage_Core_Controller_Front_Action {
    
    public function indexAction() {
        $this->loadLayout()->renderLayout();
    }

 //    protected function _isAllowed() {
	// 	return Mage::getSingleton('admin/session')->isAllowed('villa_core/villa_heronavigator');
	// }

}