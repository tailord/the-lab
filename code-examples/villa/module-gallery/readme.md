## Details

Custom Magento 1.x module used by employees to navigate the media drive to navigate VILLA's images by product style number. Users had to be logged into the Magento admin to access this.