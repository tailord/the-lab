const main = async () => {
	const gameContractFactory = await hre.ethers.getContractFactory('MyEpicGame');
	const gameContract = await gameContractFactory.deploy(
		['Leo', 'Aang', 'Pikachu', 'Neo', 'Charmandar', 'Link'], // Names
		[
			'https://i.imgur.com/pKd5Sdk.png', // Images
			'https://i.imgur.com/xVu4vFL.png',
			'https://i.imgur.com/WMB6g9u.png',
			'https://i.imgur.com/aUY4HRe.jpeg',
			'https://i.imgur.com/BWkr3dub.jpg',
			'https://i.imgur.com/aQuNaEtb.jpg',
		],
		[100, 200, 300, 500, 300, 400], // HP values
		[100, 50, 25, 200, 30, 125], // Attack damage values
		['', '', '', '', '', 'Sword'], // Items
		'Bowser', // Boss name
		'https://i.imgur.com/a9Nnp5w.jpeg', // Boss image
		10500, // Boss HP
		75 // Boss attack
	);
	await gameContract.deployed();
	console.log('Contract deployed to:', gameContract.address);
};

const runMain = async () => {
	try {
		await main();
		process.exit(0);
	} catch (error) {
		console.log(error);
		process.exit(1);
	}
};

runMain();
