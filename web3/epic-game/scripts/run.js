const main = async () => {
	const gameContractFactory = await hre.ethers.getContractFactory('MyEpicGame');
	const gameContract = await gameContractFactory.deploy(
		['Leo', 'Aang', 'Pikachu', 'Neo', 'Charmandar', 'Link'], // Names
		[
			'QmbW1kL6SRag1JDsHT6fjrnof8a8q3T6iRfNSafGdLiDVq', // Images
			'QmUQbZdHS27VzgNafQf57x2qeDbboE1iBnuRH4DwmaS7c1',
		],
		[100, 200, 300, 500, 300, 400], // HP values
		[100, 50, 25, 200, 30, 125], // Attack damage values
		['', '', '', '', '', 'Sword'], // Items
		'Bowser', // Boss name
		'https://i.imgur.com/a9Nnp5w.jpeg', // Boss image
		10500, // Boss HP
		75 // Boss attack
	);
	await gameContract.deployed();
	console.log('Contract deployed to:', gameContract.address);

	let txn;
	// We only have six characters.
	// an NFT w/ the character at index 2 of our array.
	txn = await gameContract.mintCharacterNFT(2);
	await txn.wait();

	txn = await gameContract.attackBoss();
	await txn.wait();

	txn = await gameContract.attackBoss();
	await txn.wait();

	// Get the value of the NFT's URI.
	let returnedTokenUri = await gameContract.tokenURI(1);
	console.log('Token URI:', returnedTokenUri);
};

const runMain = async () => {
	try {
		await main();
		process.exit(0);
	} catch (error) {
		console.log(error);
		process.exit(1);
	}
};

runMain();
