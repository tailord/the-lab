const CONTRACT_ADDRESS = '0x4e3Cec38fB3609f0E03444fE988936919E12A700';

const transformCharacterData = (characterData) => {
  return {
    name: characterData.name,
    imageURI: characterData.imageURI,
    hp: characterData.hp.toNumber(),
    maxHp: characterData.maxHp.toNumber(),
    attackDamage: characterData.attackDamage.toNumber(),
    item: characterData.item
  };
};

export {CONTRACT_ADDRESS, transformCharacterData};