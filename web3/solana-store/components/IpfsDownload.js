import React from 'react';
import useIPFS from '../hooks/useIPFS';

const IPFSDownload = ({ hash, filename, cta }) => {
	const file = useIPFS(hash, filename);

	return (
		<div>
			{file ? (
				<div className="download-component">
					<a
						className="download-button"
						href={file}
						download={filename}
						target="_blank"
					>
						{cta}
					</a>
				</div>
			) : (
				<p>Downloading file ...</p>
			)}
		</div>
	);
};

export default IPFSDownload;
