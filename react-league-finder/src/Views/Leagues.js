// Leagues.js

import React, {Component} from 'react';

class LeaguesView extends Component {
	render() {
		let leagueData = this.props.list.map((league, i) => {
			return(
				<div className="row" key={i}>
					<span>{league.name}</span>
					<span>{league.coords}</span>
					<span>${league.price}</span>
				</div>
			);
		});
		return(
			<div className="results">
				<div className="row titles">
					<span>Name</span>
					<span>Lat/Long</span>
					<span>Price</span>
				</div>
				{leagueData}
			</div>
		);
	}
}

export default LeaguesView;