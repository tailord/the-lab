import React, {Component} from 'react';
import LeaguesView from './Views/Leagues';

import './css/styles.css';

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			page: true,
			leagues: [],
			results: []
		};
		this.switchToCreatePage = this.switchToCreatePage.bind(this);
		this.switchToSearchPage = this.switchToSearchPage.bind(this);
		this.saveLeague = this.saveLeague.bind(this);
		this.searchLeague = this.searchLeague.bind(this);
	}

	switchToCreatePage() {
		this.setState({page:'create'});
	}

	switchToSearchPage() {
		this.setState({page:'search'});
	}

	saveLeague() {
		this.setState({
			leagues:this.state.leagues.concat({
				name:this.nameInput.value,
				coords:this.latlongInput.value,
				price:parseFloat(this.priceInput.value)
			})
		});
	}

	searchLeague() {
		var list = this.state.leagues;
		const latlong = this.locationInput.value;
		const r = this.milesInput.value;
		const b = this.budgetInput.value;
		var spend = b;
		var ranges = rangefinder(latlong, r);

		if(list.length > 0) {
			list.sort(function(a,b) {
				return a['price'] - b['price'];
			});
			list = list.filter(function(item) {
				let itemCoords = item['coords'].split(',');
				let itemLat = parseFloat(itemCoords[0]);
				let itemLong = parseFloat(itemCoords[1]);
				if(itemLat > ranges['top'] && itemLat < ranges['bottom']) {
					if(itemLong < ranges['left'] && itemLong > ranges['right']) {
						if(item['price'] <= spend) {
							spend -= item['price'];
							return item;
						}
					}
				}
			});
			this.setState({results:list});
		}
	}

	componentDidUpdate() {
		this.nameInput.value = '';
		this.latlongInput.value = '';
		this.priceInput.value = '';
	}

	render() {
		return (
			<div className="wrap">
				<div className="block">
					<h2>Create</h2>
					<div className="form">
						<input name="name" type="text" placeholder="Name" ref={(ref) => this.nameInput = ref} />
						<input name="latlong" type="text" placeholder="Latitude,Longitude" ref={(ref) => this.latlongInput = ref} />
						<input name="price" type="text" placeholder="Price ($)" ref={(ref) => this.priceInput = ref} />
						<button onClick={this.saveLeague}>Create</button>
					</div>
					<LeaguesView list={this.state.leagues} />
				</div>
				<div className="block">
					<h2>Search</h2>
					<div className="form">
						<input name="location" type="text" placeholder="Location (lat,long)" ref={(ref) => this.locationInput = ref} />
						<input name="miles" type="text" placeholder="Miles Away" ref={(ref) => this.milesInput = ref} />
						<input name="budget" type="text" placeholder="Budget ($)" ref={(ref) => this.budgetInput = ref} />
						<button onClick={this.searchLeague}>Search</button>
					</div>
					<LeaguesView list={this.state.results} />
				</div>
			</div>
		);
	}
}

function rangefinder(latlong, r) {
	let coords = latlong.split(',');
	let lat = parseFloat(coords[0]);
	let long = parseFloat(coords[1]);
	let df = r/69;
	let dl = df / Math.cos(lat);
	var range = {
		top:lat - df,
		bottom:lat + df,
		left:long - dl,
		right:long + dl
	}
	return range;
}

export default App;
