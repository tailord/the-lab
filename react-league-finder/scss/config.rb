require 'compass/import-once/activate'
# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
# http_path = "/skin/frontend/villa2/default/"
css_dir = "../src/css"
sass_dir = "../scss"
# images_dir = "../img"
# javascripts_dir = "/js"
# fonts_dir = "../font"

# You can select your preferred output style here (can be overridden via the command line):
# output_style = :expanded or :nested or :compact or :compressed
# output_style = :expanded
output_style = :compressed

# To enable relative paths to assets via compass helper functions. Uncomment:
relative_assets = true

# To disable debugging comments that display the original location of your selectors. Uncomment:
line_comments = true

# cache settings - update deploy_version per release update
# deploy_version = 5
# asset_cache_buster :none
# deploy_version = 20171109
# asset_cache_buster do |http_path, file|
#   # if file
#   #   file.mtime.strftime("%s")
#   # else
#     "v=#{deploy_version}"
#   # end
# end


# If you prefer the indented syntax, you might want to regenerate this
# project again passing --syntax sass, or you can uncomment this:
# preferred_syntax = :sass
# and then run:
# sass-convert -R --from scss --to sass sass scss && rm -rf sass && mv scss sass

on_stylesheet_error do
  system('afplay /System/Library/Sounds/Basso.aiff')
end